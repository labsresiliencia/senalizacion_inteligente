#include <NeoSWSerial.h>

const int LED_VERDE = 11;
const int LED_AMARILLO = 10;
const int LED_ROJO = 9;
const int BUZZER = 13;
const int RADIO_RX = 2;
const int RADIO_TX = 3;

NeoSWSerial nss( RADIO_RX, RADIO_TX );

void setup() {
  Serial.begin(9600);
  nss.begin(9600);
  pinMode(LED_VERDE, OUTPUT);
  pinMode(LED_AMARILLO, OUTPUT);
  pinMode(LED_ROJO, OUTPUT);
}

void loop() {
  if(nss.available()) { // Espera datos de radio
    char data = nss.read();
    switch(data) {
      case 'r':
        digitalWrite(LED_VERDE, LOW);
        digitalWrite(LED_AMARILLO, LOW);
        digitalWrite(LED_ROJO, HIGH);
        tone(13, 2000);
        break;
      case 'a':
        digitalWrite(LED_VERDE, LOW);
        digitalWrite(LED_AMARILLO, HIGH);
        digitalWrite(LED_ROJO, LOW);
        tone(13, 4000);
        break;
      case 'v':
        digitalWrite(LED_VERDE, HIGH);
        digitalWrite(LED_AMARILLO, LOW);
        digitalWrite(LED_ROJO, LOW);
        tone(13, 1000);
        break;
      default:
        digitalWrite(LED_VERDE, LOW);
        digitalWrite(LED_AMARILLO, LOW);
        digitalWrite(LED_ROJO, LOW);
        noTone(13);
    }
  }
}
