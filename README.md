# Señalización Inteligente para Emergencias

La Señalización Inteligente para Emergencias es
un conjunto de señales que se activan remotamente
al recibir una señal de alarma con el objetivo de
notificar a los miembros de la comunidad.

Este repositorio contiene el código fuente e 
instrucciones de armado del proyecto denominado 
"Señalización Inteligente para Emergencias" 
desarrollado como parte de los Laboratorios de
Resiliencia Comunitarios.

Prototipo elaborado por: Mario Gómez @ Hackerspace San Salvador

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Arduino UNO R3                  |        1 |
| Breadboard                      |        1 |
| LED Rojo                        |        3 |
| LED Verde                       |        3 |
| LED Amarillo                    |        3 |
| Resistencia 330Ohm              |        9 |
| Buzzer                          |        1 |
| Radio Receptor (RF-RX)          |        1 |
| Porta Bateria AA                |        1 |
| Cables Jumper                   |       23 |

## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Guía de Conexion en Breadboard
Las conexiónes que comienzan con **BB** se conectan en 
la Breadboard. Las que comienzan con **AR** se conectan 
al Arduino. Las que comienzan con **RA** se conectan al
módulo de radio.

| Componente                         |        |         |
|------------------------------------|--------|---------|
| Jumper 1 (Conectar de lado a lado) | BB-V-  | BB-V-   |
| LED Rojo 1                         | BB-J60 | BB-J61  |
| LED Rojo 2                         | BB-J55 | BB-J56  |
| LED Rojo 3                         | BB-J50 | BB-J51  |
| LED Amarillo 1                     | BB-J45 | AR-J46  |
| LED Amarillo 2                     | BB-J40 | BB-J41  |
| LED Amarillo 3                     | BB-J35 | BB-J36  |
| LED Verde 1                        | BB-J30 | BB-J31  |
| LED Verde 2                        | BB-J25 | BB-J26  |
| LED Verde 3                        | BB-J20 | BB-J21  |
| Resistencia 1                      | BB-G61 | BB-E61  |
| Resistencia 2                      | BB-G56 | BB-E56  |
| Resistencia 3                      | BB-G51 | BB-E51  |
| Resistencia 4                      | BB-G46 | BB-E46  |
| Resistencia 5                      | BB-G41 | BB-E41  |
| Resistencia 6                      | BB-G36 | BB-E36  |
| Resistencia 7                      | BB-G31 | BB-E31  |
| Resistencia 8                      | BB-G26 | BB-E26  |
| Resistencia 9                      | BB-G21 | BB-E21  |
| Jumper 1                           | BB-I60 | BB-V-   |
| Jumper 2                           | BB-I55 | BB-V-   |
| Jumper 3                           | BB-I50 | BB-V-   |
| Jumper 4                           | BB-I45 | BB-V-   |
| Jumper 5                           | BB-I40 | BB-V-   |
| Jumper 6                           | BB-I35 | BB-V-   |
| Jumper 7                           | BB-I30 | BB-V-   |
| Jumper 8                           | BB-I25 | BB-V-   |
| Jumper 9                           | BB-I20 | BB-V-   |
| Jumper 10                          | BB-C21 | BB-C26  |
| Jumper 11                          | BB-C36 | BB-C41  |
| Jumper 12                          | BB-C51 | BB-C56  |
| Jumper 13                          | BB-B26 | BB-B31  |
| Jumper 14                          | BB-B41 | BB-B46  |
| Jumper 15                          | BB-B56 | BB-B61  |
| Jumper 16                          | BB-A31 | AR-11   |
| Jumper 17                          | BB-A46 | AR-10   |
| Jumper 18                          | BB-A61 | AR-9    |
| Jumper 19                          | BB-V-  | AR-GND  |
| Jumper 20                          | BB-V+  | AR-5V   |
| Buzzer                             | BB-V-  | AR-13   |
| Porta Batería                      | BB-V-  | AR-VIN  |
| Jumper 21                          | BB-V-  | RA-GND  |
| Jumper 22                          | BB-V+  | RA-VCC  |
| Jumper 23                          | AR-2   | RA-DAT  |

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com) 
y únete al canal ***#propuesta_d2***.
